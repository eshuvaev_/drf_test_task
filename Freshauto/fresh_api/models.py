from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Car(models.Model):
    
    brand = models.CharField("brand", max_length=255)
    model = models.CharField("model", max_length=255)
    power = models.IntegerField("power")
    color = models.CharField("color", max_length=255)
    price = models.IntegerField("price")
    release_year = models.IntegerField("release_year")

    user = models.ForeignKey(User, on_delete=models.CASCADE)

