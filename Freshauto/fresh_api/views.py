from rest_framework import generics, permissions, serializers
from rest_framework.permissions import IsAuthenticated
from fresh_api.models import Car
from fresh_api.serializers import CarDetailSerializer, CarsListSerializer
from fresh_api.permissions import IsOwnerOrReadOnly

# Create your views here.

class CarCreateView(generics.CreateAPIView):
    serializer_class = CarDetailSerializer

class CarsListView(generics.ListAPIView):
    serializer_class = CarsListSerializer
    queryset = Car.objects.all()
    permission_classes = (IsAuthenticated, )


class CarDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CarDetailSerializer
    queryset = Car.objects.all()
    permission_classes = (IsOwnerOrReadOnly, )
