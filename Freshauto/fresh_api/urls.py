from django.contrib import admin
from django.urls import path, include
from fresh_api.views import *

app_name = 'fresh_api'
urlpatterns = [
    path('car/create/', CarCreateView.as_view()),
    path('car/list/', CarsListView.as_view()),
    path('car/detail/<int:pk>/', CarDetailView.as_view()),
    
]
